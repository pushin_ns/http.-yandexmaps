import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {

    private Logger logger = LoggerFactory.getLogger(App.class);
    private static final String URL = "https://yandex.ru/maps";
    private static final String ADDRESS = "Ижевск, Карла Маркса, 218";

    public static void main(String[] args) throws IOException {

        App app = new App();
        HttpURLConnection con = app.getConnection(URL);
        String csrf = app.searchForCsrf(con);
        String yandexUid = app.searchForYandexUid(con);
        app.logger.info("Координаты для адреса {}: {}", ADDRESS, app.getCoordinates(csrf, yandexUid));
    }

    private HttpURLConnection getConnection(String URL) throws IOException {
        URL url = new URL(URL);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("GET");

        con.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
        con.setRequestProperty("Connection", "keep-alive");
        con.setRequestProperty("Host", "yandex.ru");
        con.setRequestProperty("Upgrade-Insecure-Requests", "1");
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");

        return con;
    }

    private String getCoordinates(String csrf, String uid) throws IOException {

        String dynamicUrl = URL + "/api/search?text=" + URLEncoder.encode(ADDRESS, "UTF-8") + "&lang=ru_RU&csrfToken=" + csrf;
        HttpURLConnection con = this.getConnection(dynamicUrl);
        con.setRequestProperty("Cookie", "yandexuid=" + uid);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            sb.append(inputLine);
        }
        in.close();
        Pattern mainInfoPattern = Pattern.compile("InternalToponymInfo[^]]++");
        Matcher m = mainInfoPattern.matcher(sb);
        String internalToponymInfoString = "";
        if (m.find())
            internalToponymInfoString += m.group();
        return internalToponymInfoString.substring(internalToponymInfoString.indexOf('[') + 1);

    }

    private String searchForCsrf(HttpURLConnection con) throws IOException {
        Pattern csrfPattern = Pattern.compile("csrfToken\":\"[^\"]++");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            Matcher matcher = csrfPattern.matcher(inputLine);
            if (matcher.find()) {
                logger.info("CSRF Token - {}", matcher.group().substring(12));
                return matcher.group().substring(12);
            }
        }
        in.close();
        return null;
    }

    private String searchForYandexUid(HttpURLConnection con) {
        Pattern uidPattern = Pattern.compile("yandexuid=[^;]++");

        for (int i = 0; i < con.getHeaderFields().size(); i++) {
            Matcher matcher = uidPattern.matcher(con.getHeaderField(i));
            if (matcher.find()) {
                logger.info("YandexUID - {}", matcher.group().substring(10));
                return matcher.group().substring(10);
            }
        }
        return null;
    }

}
